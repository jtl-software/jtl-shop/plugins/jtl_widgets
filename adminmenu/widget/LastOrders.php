<?php declare(strict_types=1);

namespace Plugin\jtl_widgets;

use JTL\Router\Controller\Backend\OrderController;
use JTL\Shop;
use JTL\Widgets\AbstractWidget;

/**
 * Class LastOrders
 * @package Plugin\jtl_widgets
 */
class LastOrders extends AbstractWidget
{
    /**
     * @inheritDoc
     */
    public function getContent(): string
    {
        if (\method_exists($this, 'setPermission')) {
            $this->setPermission('ORDER_VIEW');
        }
        $paths      = $this->oPlugin->getPaths();
        $controller = new OrderController(
            $this->getDB(),
            Shop::Container()->getCache(),
            Shop::Container()->getAlertService(),
            Shop::Container()->getAdminAccount(),
            Shop::Container()->getGetText()
        );
        $orders     = $controller->getOrders(' LIMIT 10', '');

        return $this->getSmarty()
            ->assign('cDetail', $paths->getAdminPath() . '/widget/templates/lastOrdersDetail.tpl')
            ->assign('orders', $orders)
            ->assign(
                'cDetailPosition',
                $paths->getAdminPath() . '/widget/templates/lastOrdersDetailPosition.tpl'
            )
            ->assign('cAdminmenuPfadURL', $paths->getAdminURL())
            ->fetch(__DIR__ . '/templates/widgetLastOrders.tpl');
    }
}
