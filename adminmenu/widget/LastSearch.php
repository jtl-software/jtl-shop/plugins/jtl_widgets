<?php declare(strict_types=1);

namespace Plugin\jtl_widgets;

use JTL\Widgets\AbstractWidget;

/**
 * Class LastSearch
 * @package Plugin\jtl_widgets
 */
class LastSearch extends AbstractWidget
{
    /**
     * @inheritDoc
     */
    public function init(): void
    {
        if (\method_exists($this, 'setPermission')) {
            $this->setPermission('MODULE_LIVESEARCH_VIEW');
        }

        $lastQueries = $this->getDB()->getObjects(
            'SELECT * 
                FROM tsuchanfrage 
                ORDER BY dZuletztGesucht DESC 
                LIMIT 10'
        );
        $this->getSmarty()->assign('lastQueries', $lastQueries);
    }

    /**
     * @inheritDoc
     */
    public function getContent(): string
    {
        return $this->getSmarty()->fetch(__DIR__ . '/templates/widgetLastSearch.tpl');
    }
}
