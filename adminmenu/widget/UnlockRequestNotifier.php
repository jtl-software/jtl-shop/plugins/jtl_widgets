<?php declare(strict_types=1);

namespace Plugin\jtl_widgets;

use JTL\DB\SqlObject;
use JTL\Router\Controller\Backend\ActivationController;
use JTL\Shop;
use JTL\Widgets\AbstractWidget;
use stdClass;

/**
 * Class UnlockRequestNotifier
 * @package Plugin\jtl_widgets
 */
class UnlockRequestNotifier extends AbstractWidget
{
    /**
     * @inheritDoc
     */
    public function init(): void
    {
        if (\method_exists($this, 'setPermission')) {
            $this->setPermission('UNLOCK_CENTRAL_VIEW');
        }
        $sql          = '';
        $searchSQL    = new SqlObject();
        $groups       = [];
        $requestCount = 0;
        $controller   = new ActivationController(
            $this->getDB(),
            Shop::Container()->getCache(),
            Shop::Container()->getAlertService(),
            Shop::Container()->getAdminAccount(),
            Shop::Container()->getGetText()
        );

        $group                = new stdClass();
        $group->cGroupName    = \__('Customer reviews');
        $group->kRequestCount = \count($controller->getReviews($sql, $searchSQL));
        $groups[]             = $group;
        $requestCount        += $group->kRequestCount;

        $searchSQL->setOrder(' dZuletztGesucht DESC ');
        $group                = new stdClass();
        $group->cGroupName    = \__('Search queries');
        $group->kRequestCount = \count($controller->getSearchQueries($sql, $searchSQL));
        $groups[]             = $group;
        $requestCount        += $group->kRequestCount;

        $group                = new stdClass();
        $group->cGroupName    = \__('News comments');
        $group->kRequestCount = \count($controller->getNewsComments($sql, $searchSQL));
        $groups[]             = $group;
        $requestCount        += $group->kRequestCount;

        $searchSQL->setOrder(' tnewsletterempfaenger.dEingetragen DESC ');
        $group                = new stdClass();
        $group->cGroupName    = \__('Newsletter recipients');
        $group->kRequestCount = \count($controller->getNewsletterRecipients($sql, $searchSQL));
        $groups[]             = $group;
        $requestCount        += $group->kRequestCount;

        $this->getSmarty()->assign('groups', $groups)
            ->assign('requestCount', $requestCount);
    }

    /**
     * @inheritDoc
     */
    public function getContent(): string
    {
        return $this->getSmarty()->fetch(__DIR__ . '/templates/widgetUnlockRequestNotifier.tpl');
    }
}
